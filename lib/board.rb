class Board
  attr_reader :grid

  def initialize(grid = Board.default_grid) #Why couldn't the *Board* be replaced with *self* ?
    @grid = grid
  end

  def self.default_grid
    @grid = (Array.new(10) { Array.new(10)})
  end

  #Because of the following two methods, we can now use this notation:
  #      board[pos] = :X  or  board[pos] #=> :X
  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end

  def display
    p @grid[0]
    p @grid[1]
    p @grid[2]
    p @grid[3]
    p @grid[4]
    p @grid[5]
    p @grid[6]
    p @grid[7]
    p @grid[8]
    p @grid[9]
  end

  def count
    count = 0
    @grid.each {|array| array.each { |ele| count += 1 if ele == :s}}
    count
  end

  def populate_grid
    3.times { place_random_ship}
  end

  def in_range?(pos)

  end

  def empty?(pos=nil)
    return true if won? && pos == nil
    return false if @grid.any? {|array| array.any? {|a| a == :s}} && pos == nil
    self[pos] == nil
  end

  def full?
    count = 0
    @grid.each {|array| array.each { |ele| count += 1 if ele == nil}}
    count == 0
  end

  def place_random_ship
    if full?
      raise Exception
    else
      rand_row = rand(@grid.length)
      rand_col = rand(@grid[0].length)
      pos = [rand_row,rand_col]

      until empty?(pos)
        rand_row = rand(@grid.length)
        rand_col = rand(@grid[0].length)
        pos = [rand_row,rand_col]
      end

      @grid[rand_row][rand_col] = :s
    end
  end

  def won?
    count = 0
    @grid.each {|array| array.each { |ele| count += 1 if ele == :s}}
    count == 0
  end
end
