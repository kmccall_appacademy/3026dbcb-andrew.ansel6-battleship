require "rspec"
require "battleship"
require "board"
require "player"

class BattleshipGame
  attr_reader :board, :player

  def initialize(player, board)
    @board = board
    @player = player
  end

  def attack(pos)
    if @board[pos] == :s
      @board[pos] = :x
      puts "Hit!"
      puts ""
    elsif @board.empty?(pos)
      @board[pos] = :x
      puts "Miss."
      puts ""
    else
      puts "This position has already been attacked."
      puts ""
    end
  end

  def count
    @board.count
  end

  def game_over?
    @board.won?
  end

  def play_turn
    pos = @player.get_play
    attack(pos)
  end

  def play
    @board.populate_grid

    until game_over?
      @board.display
      play_turn
    end

    puts "Congrats, you sank all of the opponents battleships!"
  end
end

# #Use this to test: 
# player = HumanPlayer.new("AJ")
# board = Board.new
# bs = BattleshipGame.new(player, board)
# bs.play
